#import <Foundation/Foundation.h>

@interface IDSTile : NSObject

/**
 * @brief Unique identifier for one tile.
 *
 * @param tileID ID as long.
 */
@property (nonatomic) NSNumber *tileID;

/**
 * @brief Array of tiles.
 */
@property (nonatomic, strong) NSArray *tiles;

/**
 * @brief Mm per pixel of this tile size.
 *
 * @param mmPerPixel milimeter per pixel as float.
 */
@property (nonatomic) NSNumber *mmPerPixel;

/**
 * @brief The tile size (a power of 2). 1 is the most detailed tile size.
 *
 * @param tileSize Tilesize as int.
 */
@property (nonatomic) NSUInteger tileSize;

/**
 * @brief Height of the map with this tile size in pixel.
 *
 * @param summaryPixelHeight Height value as int.
 */
@property (nonatomic) NSUInteger summaryPixelHeight;

/**
 * @brief Width of the map with this tile size in pixel.
 *
 * @param summaryPixelWidth Width as int.
 */
@property (nonatomic) NSUInteger summaryPixelWidth;

/**
 * @brief Number of horizontal tiles.
 *
 * @param countHorziontalTiles Number of horizontal tiles as int.
 */
@property (nonatomic) NSUInteger countHorizontalTiles;

/**
 * @brief Number of vertical tiles.
 *
 * @param countVerticalTiles Number of vertical tiles as int.
 */
@property (nonatomic) NSUInteger countHVerticalTiles;

@end
