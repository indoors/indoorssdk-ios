#import <Foundation/Foundation.h>


#define IndoorsErrorDomain @"IndoorsErrorDomain"

typedef NS_ENUM(NSInteger, IndoorsErrorCode) {
    IndoorsErrorCodeGeneralBuildingNotFound,
    IndoorsErrorCodeAuthLicenseExceeded,
    IndoorsErrorCodeAuthLicenseNotExisting,
    IndoorsErrorCodeAuthAlreadyRegistered,
    IndoorsErrorCodeAuthObligatoryIORequest,
    IndoorsErrorCodeLoadOnlineBuildingsFailed,
    IndoorsErrorCodeLoadBuildingFailed,
    IndoorsErrorCodeLoadBuildingFailedUnzip,
    IndoorsErrorCodeLoadBuildingFailedDownloading
};

@class IndoorsError;

@protocol IndoorsErrorDelegate <NSObject>

/**
 * @brief Delegate method for Indoors Error.
 *
 * @param indoorsException
 */
- (void)onError:(IndoorsError *)indoorsError;

@end

@interface IndoorsError : NSObject

@property(nonatomic) IndoorsErrorCode errorCode;

- (id)initWithErrorCode:(IndoorsErrorCode)error;

@end