#import <Foundation/Foundation.h>

@interface IndoorsException : NSException

+ (void)raiseWithMessage:(NSString *)message;
+ (void)raise:(NSString*)name andMessage:(NSString *)message;

@end
