#import <Foundation/Foundation.h>

#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>

@class IDSFloor;
@class IDSBuilding;

@interface IDSDefaultMap : NSObject

@property (nonatomic, weak) IDSBuilding *building;

@property (nonatomic) NSNumber *defaultMapID;
@property (nonatomic) NSNumber *mmPerPixelBase;
@property (nonatomic) NSInteger maxTileSize;
@property (nonatomic, strong) IDSFloor *floor;

//= new HashMap<Integer, Tiles>()
@property (nonatomic,strong) NSMutableDictionary *tiles;

//private Map<String, ResourceLinkImpl> links;
@property (nonatomic,strong) NSDictionary *links;

/**
 * Unique identifier of the map.
 */
@property (nonatomic) NSNumber *mapID;

/**
 * Highest tile size available.
 *
 * @return
 */
@property (nonatomic) NSInteger tileSizeMaximum;

/**
 * Mm per pixel for tile size 1.
 */
@property (nonatomic) NSNumber *meterPerPixel;

@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, assign, readonly) CGSize mapSize;
@property (nonatomic, assign, readonly) NSUInteger levelsOfDetail;

- (UIImage *)tileForScale:(CGFloat)scale row:(NSInteger)row col:(NSInteger)col;

- (void)calculateValues;

@end
