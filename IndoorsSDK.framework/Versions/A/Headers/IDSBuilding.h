#import <Foundation/Foundation.h>
#import "IDSFloor.h"
#import "IDSZone.h"

typedef NS_OPTIONS(NSUInteger, IDSBuildingSource) {
    IDSBuildingSourceCache = 1 << 0,
    IDSBuildingSourceBundle = 1 << 1,
    IDSBUildingSourceOnline = 1 << 2
};

/**
 * @brief Model class representing a building.
 *
 * Note: Buildings retrieved with getOnlineBuildings will not be fully
 * initialized with all their properties. Floor or tile properties will not be set as
 * they are intended to show all available buildings.
 *
 * Buildings retrieved from getBuilding will have all their members set.
 */
@interface IDSBuilding : NSObject

@property (nonatomic) IDSBuildingSource source;
@property (nonatomic) NSMutableDictionary *floors;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *buildingDescription;
@property (nonatomic) NSNumber *latOrigin;
@property (nonatomic) NSNumber *lonOrigin;
@property (nonatomic) NSNumber *rotation;
@property (nonatomic) NSUInteger buildingID;
@property (nonatomic) NSString *databasePath;

- (NSInteger)getInitialFloorLevel;
- (IDSFloor *)floorAtLevel:(NSInteger)floorLevel;
- (IDSFloor *)getFloorById:(NSNumber *)floorId;
- (IDSZone *)getZoneById:(NSNumber *)zoneId;

@end
