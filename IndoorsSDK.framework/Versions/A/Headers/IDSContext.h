#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, IndoorsContext) {
    IndoorsContextStationary,
    IndoorsContextMoving,
    IndoorsContextWalking
};

typedef NS_ENUM(NSUInteger, IDSState) {
    IDSStateBegan,
    IDSStateChanged,
    IDSStateEnded
};

@interface IDSContext : NSObject

@property (nonatomic) IndoorsContext context;
@property (nonatomic) IDSState state;

@end
