#import <Foundation/Foundation.h>

@class IDSDefaultMap;
@interface IDSFloor : NSObject

/**
 * @brief Floor id of the building.
 */
@property (nonatomic) NSNumber *floorID;

/**
 * @brief Name of the building.
 */
@property (nonatomic, strong) NSString *name;

/**
 * @brief Default map object. There may be other maps (just visually different).
 */
@property (nonatomic, strong) IDSDefaultMap *defaultMap;

/**
 * @brief Width of this building in mm.
 */
@property (nonatomic) NSInteger widthInMeter;

/**
 * @brief Length of this building in mm.
 */
@property (nonatomic) NSInteger lengthInMeter;

/**
 * @brief Floor level. I.e.: ground-floor = 0
 */
@property (nonatomic) NSInteger level;

/**
 * @brief Space from left origin of ground floor if this floor has other dimensions.
 */
@property (nonatomic) NSInteger leftOriginInMeter;

/**
 * @brief Zones available in floor
 */
@property (nonatomic,strong) NSMutableArray *zones;

/**
 * @brief Space from top origin of ground floor if this floor has other dimensions.
 */
@property (nonatomic) NSInteger topOriginInMeter;
@property (nonatomic) NSString *floorDescription;

@property (nonatomic) NSInteger mmWidth;
@property (nonatomic) NSInteger mmHeight;

@end
