#import <Foundation/Foundation.h>

@class IDSBuilding;
@class IndoorsError;

/**
 * Delegate to retrieve buildings available from indoo.rs server.
 */
@protocol OnlineBuildingDelegate <NSObject>

/**
 * @brief List of buildings available from indoo.rs server.
 *
 * @param buildings Array of IDSBuilding objects. The objects do not contain floors and tiles.
 */
- (void)setOnlineBuildings:(NSArray *)buildings;

- (void)getOnlineBuildingsFailWithError:(NSError *)error;

@end
    
/**
 * @brief Delegate to retrieve calculated route.
 */
@protocol RoutingDelegate <NSObject>

/**
 * @brief Called when requested route has been calculated.
 *
 * @param path Contains IDSCoordinate objects representing the path.
 */
- (void)setRoute:(NSArray *)path;

@end

/**
 * @brief Delegate for progress of loading a building.
 */
@protocol LoadingBuildingDelegate <NSObject>

/**
 * @brief Consequent calls are made until the building is loaded.
 *
 * @param progress Progress measured in percent.
 */
@optional
- (void)loadingBuilding:(NSNumber *)progress;
        
/**
 * @brief Service finished processing building.
 *
 * @param building The loaded building.
 */
@required
- (void)buildingLoaded:(IDSBuilding *)building;

/**
 * @brief Callback when loading building failed
 */
@required
- (void)loadingBuildingFailed;

@end


typedef NS_ENUM(NSUInteger, IDSBluetoothState) {
    IDSBluetoothStateAvailable,
    IDSBluetoothStateUnavailable
};

typedef NS_ENUM(NSUInteger, IDSLocationAuthorizationStatus) {
    IDSLocationAuthorizationStatusAllowed,
    IDSLocationAuthorizationStatusNotAllowed
};

/**
 * @brief Retrieve information about the authentication status.
 */
@protocol IndoorsServiceDelegate <NSObject>

/**
 * @brief Exceptions from this method will always have an error code set. See IndoorsErrorCode.h for details.
 */
- (void)onError:(IndoorsError *)indoorsError;
/**
 * @brief Triggered as soon as bluetooth status has changed.
 */
- (void)bluetoothStateDidChange:(IDSBluetoothState)bluetoothState;

/**
 * @brief Triggered when location authorization status has changed.
 */
- (void)locationAuthorizationStatusDidChange:(IDSLocationAuthorizationStatus)status;

@end

