#import <Foundation/Foundation.h>
#import "IndoorsError.h"

@class IDSBuilding;

@interface IDSBuildingManager : NSObject

@property (readonly, copy) NSSet *loadedBuildings;

- (void)loadBuildingWithId:(NSUInteger)buildingId
                  progress:(void (^)(NSInteger progress, NSInteger currentStep, NSInteger totalSteps, NSString *phase))progress
                completion:(void (^)(IDSBuilding *building, IndoorsError *error))completion;

- (void)unloadBuilding:(IDSBuilding *)building;
- (void)unloadAllBuildings;
- (void)cancelLoadBuilding;

@end
