#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, IDSZoneType) {
    IDSZoneTypePOI,
    IDSZoneTypeDeadZone,
    IDSZoneTypeInbound
};

@class UIBezierPath;

@interface IDSZone : NSObject

@property (nonatomic) NSNumber *zone_id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) NSUInteger floor_id;
@property (nonatomic, strong) NSString *zoneDescription;
@property (nonatomic, strong) NSMutableArray *points;

- (UIBezierPath *)polygonPath;

@end
