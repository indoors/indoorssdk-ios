#import <UIKit/UIKit.h>

#import "ISMapOverlay.h"

typedef NS_ENUM(NSUInteger, ISRouteMapOverlayLineCap) {
    ISRouteMapOverlayLineCapButt,
    ISRouteMapOverlayLineCapRound
};

@interface ISRouteMapOverlay : ISMapOverlay

@property (nonatomic) ISRouteMapOverlayLineCap lineCap;

- (instancetype)initWithPath:(NSArray *)path color:(UIColor *)color lineWidth:(CGFloat)lineWidth floorLevel:(NSInteger)floorLevel;

@end
