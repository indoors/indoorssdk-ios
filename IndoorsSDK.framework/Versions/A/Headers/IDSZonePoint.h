#import <Foundation/Foundation.h>

@interface IDSZonePoint : NSObject
@property (nonatomic) NSInteger zonepoint_id;
@property (nonatomic) NSInteger x;
@property (nonatomic) NSInteger y;
@property (nonatomic) NSInteger sortOrder;
@end
