#import <Foundation/Foundation.h>
#import "IndoorsError.h"

@protocol IDSServiceAuthenticationDelegate

- (void)serviceAuthenticationSuccessForLicenseKey:(NSString*)licenseKey;
- (void)serviceAuthenticationFailForLicenseKey:(NSString*)licenseKey withError:(IndoorsError*)error;

@end

@interface IDSServiceAuthentication : NSObject

@property (nonatomic, strong) id<IDSServiceAuthenticationDelegate> delegate;

- (void)authWithLicenseKey:(NSString*)licenseKey;

@end
