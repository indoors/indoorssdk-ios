#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, IDSLogLevel) {
    IDSLogLevelInfo,
    IDSLogLevelWarning,
    IDSLogLevelError,
    IDSLogLevelFatal
};

@interface IDSLog : NSObject

@end
