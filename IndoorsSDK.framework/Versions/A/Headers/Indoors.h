#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "IndoorsError.h"
#import "IDSLog.h"
#import "IndoorsDelegate.h"
#import "IDSBuilding.h"
#import "IDSContext.h"
#import "IDSZone.h"
#import "IDSZonePoint.h"
#import "IDSServiceAuthentication.h"
#import "IndoorsLocationListener.h"
#import "IDSBuildingManager.h"

@class IDSCoordinate;

typedef NS_ENUM(NSInteger, IndoorsParameterKey) {
    IndoorsParameterKeyForceMapLocationMode = 0,
    IndoorsParameterKeyPositionUpdateInterval = 5
};

typedef NS_ENUM(NSInteger, IndoorsParameterValue) {
    IndoorsParameterValueForceMapLocationModeImprovement
};

@interface Indoors : NSObject

+ (Indoors *)instance;

/**
 * @brief Initializes indoo.rs-SDK.
 *
 * @param licenseKey Your indoo.rs API key.
 * @param serviceDelegate Service delegate to be called on success or failure.
 *
 * @result indoo.rs instance - You should not use it to call any API, instead use the shared instance.
 */
- (Indoors *)initWithLicenseKey:(NSString *)licenseKey andServiceDelegate:(id<IndoorsServiceDelegate>)serviceDelegate;

/**
 * @brief Initializes indoo.rs-SDK.
 *
 * @param licenseKey Your indoo.rs API key.
 * @param serviceDelegate Service delegate to be called on success or failure.
 * @param offlineMode If set to TRUE, all internet communiction will be disabled.
 *
 * @result indoo.rs instance - You should not use it to call any API, instead use the shared instance.
 */
- (Indoors *)initWithLicenseKey:(NSString *)licenseKey andServiceDelegate:(id<IndoorsServiceDelegate>)serviceDelegate andOfflineMode:(BOOL)offlineMode;

- (void)setLogLevel:(IDSLogLevel)level;

/**
 * @brief Validates licenseKey passed to initWithLicenseKey:andServiceDelegate.
 */
- (void)validateLicenseKeyWithDelegate:(id<IDSServiceAuthenticationDelegate>)delegate;

/**
 * @brief Register location listener to recieve location updates.
 */
- (void)registerLocationListener:(id<IndoorsLocationListener>)listener;

/**
 * @brief Remove location listener.
 */
- (void)removeLocationListener:(id<IndoorsLocationListener>)listener;

- (void)setParameter:(IndoorsParameterValue)value forKey:(IndoorsParameterKey)key;
- (void)setParameterObject:(id)value forKey:(IndoorsParameterKey)key;

- (void)initializeLocalizationWithBuilding:(IDSBuilding*)building success:(void (^)())success;
- (void)stopLocalization;
- (void)startLocalization;

- (void)getBuilding:(IDSBuilding *)building forRequestDelegate:(id<LoadingBuildingDelegate>)delegate startLocalization:(BOOL)startLocalization;
- (void)getBuilding:(IDSBuilding *)building forRequestDelegate:(id<LoadingBuildingDelegate>)delegate;
- (void)cancelGetBuilding;

- (void)getOnlineBuildings:(id<OnlineBuildingDelegate>)onlineBuildingDelegate;

- (CLLocation *)convertCoordinateToLocation:(IDSCoordinate *)coordinate;

- (void)routeFromLocation:(IDSCoordinate *)from toLocation:(IDSCoordinate *)to inBuilding:(IDSBuilding *)building delegate:(id<RoutingDelegate>)routingDelegate;
- (void)routeFromLocation:(IDSCoordinate *)from toLocation:(IDSCoordinate *)to delegate:(id<RoutingDelegate>)routingDelegate;

- (void)enableEvaluationMode:(BOOL)isEvaluationModeEnabled;

- (void)enableRouteSnappingWithRoute:(NSArray *)route;
- (void)setRouteSnappingMaxDistance:(NSNumber *)maxDistance;
- (void)disableRouteSnapping;

- (void)enablePredefinedRouteSnapping;
- (void)setPredefinedRouteSnappingMaxDistance:(NSNumber *)maxDistance;
- (void)disablePredefinedRouteSnapping;

- (void)setSmallJumpDistance:(NSNumber*)distance __deprecated;
- (void)setBigJumpDistance:(NSNumber*)distance __deprecated;

- (void)useSensors:(BOOL)use __deprecated;
- (void)setDutyCycle:(int)dutyCycle __deprecated;

- (NSString*)getIndoorsDeviceId;

- (void)setAlwaysDownloadNewBuildingVersion:(BOOL)shouldAlways __deprecated;
- (void)setEnableKalmanFilter:(BOOL)enabled __deprecated;
- (void)setEnableStabilisationFilter:(BOOL)enabled __deprecated;
- (void)setStabilisationFilterTime:(NSInteger)stabilisationFilterTime __deprecated;
- (void)setDeadZoneFilterEnabled:(BOOL)enabled __deprecated;
- (void)setBackJumpFilterEnabled:(BOOL)enabled;
- (void)setPortalCheckEnabled:(BOOL)enabled;
- (void)setPressureCheckEnabled:(BOOL)enabled;
- (void)setGpsFallbackFilterEnabled:(BOOL)enabled;
- (void)setVerboseLoggingEnabled:(BOOL)enabled;
- (void)setAccuracyCalculationEnabled:(BOOL)enabled;
- (void)setLoadTiles:(BOOL)loadTiles;

@end
