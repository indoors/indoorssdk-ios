#import <Foundation/Foundation.h>

@interface IndoorsConfiguration : NSObject

+ (IndoorsConfiguration *)sharedInstance;

@property (nonatomic, strong) NSString *sdkVersion;
@property (nonatomic, strong) NSString *licenseKey;
@property (nonatomic, strong) NSArray *features;
@property (nonatomic, strong) NSString *endPoint;
@property (nonatomic, strong) NSString *recordingEndPoint;
@property (nonatomic, strong) NSDate *licenseExpirationDate;
@property (nonatomic) BOOL forceOfflineMode;

@property (nonatomic) NSTimeInterval batchedRequestTimeInterval;
/**
 * @brief When set to NO, SDK will use cached building instead of trying to downloading newer version from the server.
 *
 * @default YES
 */
@property (nonatomic) BOOL alwaysDownloadNewBuildingVersion;

/**
 * @brief Kalman filter is used to improve localisation accuracy for certain setups. Contact support@indoo.rs to get more information about your venue.
 *
 * @default YES
 */
@property (nonatomic, getter = isKalmanFilterEnabled) BOOL kalmanFilterEnabled;

/**
 * @brief Stabilisation filter is used to reduce user location jumps when you are not walking.
 *
 * @default NO
 */
@property (nonatomic, getter = isStabilizationFilterEnabled) BOOL stabilizationFilterEnabled;

/**
 * @brief The number of miliseconds it takes for the stabilisation filter to kick in. Time range is 4000-20000 [ms]
 *
 * @default 4000 [ms]
 */
@property (nonatomic) NSInteger stabilizationFilterTime;

/**
 * @brief used to avoid location jumps in the opposite direction of moving.
 *
 * @default NO
 */
@property (nonatomic, getter = isBackJumpFilterEnabled) BOOL backJumpFilterEnabled;

/**
 * @brief used to avoid fake floor changes based on the distance to portals.
 *
 * @default YES
 */
@property (nonatomic, getter = isPortalCheckEnabled) BOOL portalCheckEnabled;

/**
 * @brief used to avoid fake floor changes based on the pressure sensor data.
 *
 * @default YES
 */
@property (nonatomic, getter = isPressureCheckEnabled) BOOL pressureCheckEnabled;

/**
 * @brief DeadZone filter is used to avoid location updates in unaccessable regions.
 *
 * @default YES
 */
@property (nonatomic, getter = isDeadZoneFilterEnabled) BOOL deadZoneFilterEnabled;

/**
 * @brief if no location could be calculated, display GPS position, if available.
 *
 * @default YES
 */
@property (nonatomic, getter = isGpsFallbackFilterEnabled) BOOL gpsFallbackFilterEnabled;

/**
 * @brief verbose logging for every position change by filters.
 *
 * @default NO
 */
@property (nonatomic, getter = isVerboseLoggingEnabled) BOOL verboseLoggingEnabled;

/**
 * @brief whether location permission dialog should be triggered by SDK automatically
 *
 * @default YES
 */
@property (nonatomic, getter = shouldTriggerLocationPermission) BOOL triggerLocationPermission;

/**
 * @brief whether accuracy should be calculated for every position
 *
 * @default YES
 */
@property (nonatomic, getter = isAccuracyCalculationEnabled) BOOL accuracyCalculationEnabled;

/** @brief if tiles for building should be downloaded together with radio map
 *
 * @default YES
 */
@property (nonatomic, getter = shouldLoadTiles) BOOL loadTiles;

/** @brief how far you can zoom the map after highest resolution tiles are used
 *
 * @default 1
 */
@property (nonatomic, getter = getMaxMapZoomLevel) double maxMapZoomLevel;

/** @brief on which server is your building stored
 *
 * @default @"api"
 *
 * For US based applications, set it to @"api-us"
 */
@property (nonatomic, getter = getServerInstance) NSString* serverInstance;

/** @brief if additional debug information should be displayed in a dedicated view
 *
 * @default NO
 */
@property (nonatomic, getter = getUseDebugStream) bool useDebugStream;

@property (nonatomic) bool debugRecordings;

/** @brief Upload positions calculated during localization (used for Analytics)
 *
 * @default YES
 */
@property (nonatomic) bool uploadPositions;
@end
