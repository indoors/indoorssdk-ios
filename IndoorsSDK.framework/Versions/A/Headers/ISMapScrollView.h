#import <UIKit/UIKit.h>
#import "IDSCoordinate.h"
#import "IDSDefaultMap.h"
#import "ISAnnotationView.h"
#import "ISMapOverlay.h"
#import "ISRouteMapOverlay.h"

@class ISMapScrollView;

typedef NS_ENUM(NSUInteger, ISZoneDisplayMode) {
    ISZoneDisplayModeUserCurrentLocation, // Only display user's current zone.
    ISZoneDisplayModeModeAllAvailable, // Display all zones.
    ISZoneDisplayModeNone // Not display any zone.
};

@protocol ISMapScrollViewDelegate <NSObject>
@optional
- (void)mapScrollView:(ISMapScrollView *)mapScrollView userDidSelectLocation:(IDSCoordinate *)location;
- (void)mapScrollView:(ISMapScrollView *)mapScrollView userDidTapLocation:(IDSCoordinate *)location;
@end

@interface ISMapScrollView : UIScrollView <UIScrollViewDelegate>

@property (nonatomic) IDSDefaultMap *map;
@property (nonatomic, weak) id<ISMapScrollViewDelegate> mapScrollViewDelegate;
@property (nonatomic) CGRect visibleMapRect;
@property (nonatomic) NSArray *currentRoutingPath;
@property (nonatomic) IDSCoordinate *userCurrentLocation;
@property (nonatomic) NSInteger userCurrentFloorLevel;

@property (nonatomic) BOOL userPositionIconIndicatesUserOrientation;
@property (nonatomic) UIImage *userPositionIcon;
@property (nonatomic) UIColor *defaultUserPositionIconColor;
@property (nonatomic) UIImage *noOrientationUserPositionIcon;
@property (nonatomic) UIColor *defaultNoOrientationUserPositionIconColor;
@property (nonatomic) UIColor *userPositionAccuracyCircleColor;
@property (nonatomic, getter=isUserLocationHidden) BOOL userLocationHidden;

- (void)letUserSelectLocationWithCalloutTitle:(NSString *)title;
- (void)requireUserToSelectLocationWithCalloutTitle:(NSString *)title;
- (void)cancelSelectLocation;

- (void)setRoutingPath:(NSArray *)path;
- (void)setRoutingPath:(NSArray *)path withColor:(UIColor *)color lineWidth:(CGFloat)lineWidth;
- (void)setRoutingPath:(NSArray *)path withColor:(UIColor *)color lineWidth:(CGFloat)lineWidth lineCap:(ISRouteMapOverlayLineCap)lineCap;
- (void)clearRouting;

- (void)setUserPosition:(IDSCoordinate *)coordinate;
- (void)setUserOrientation:(NSNumber *)orientation;
- (void)setMapCenterWithCoordinate:(IDSCoordinate *)coordinate;

- (void)setVisibleMapRect:(CGRect)mapRect animated:(BOOL)animated;

- (void)addOverlay:(ISMapOverlay *)overlay toFloorLevel:(NSInteger)floorLevel;
- (void)removeOverlay:(ISMapOverlay *)overlay;
- (void)addAnnotation:(ISAnnotationView *)annotationView;
- (void)removeAnnotation:(ISAnnotationView *)annotationView;
- (void)setZoneDisplayMode:(ISZoneDisplayMode)zoneDisplayMode;

- (void)didReceiveWeakSignal;

- (IDSCoordinate *)coordinateForPoint:(CGPoint)point;
- (CGPoint)pointForCoordinate:(IDSCoordinate *)coordinate;

- (void) setRoutingImagesUp:(UIImage*) routingUp andDown: (UIImage *) routingDown;

@end
