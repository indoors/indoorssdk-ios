import UIKit

class ViewController: UIViewController, IndoorsServiceDelegate, ISIndoorsSurfaceViewControllerDelegate {
    
    var _indoorsSurfaceViewController: ISIndoorsSurfaceViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ = Indoors(licenseKey: "YOUR-API-KEY" , andServiceDelegate: nil)
        _indoorsSurfaceViewController = ISIndoorsSurfaceViewController()
        _indoorsSurfaceViewController!.delegate = self
        
        addSurfaceAsChildViewController()
        
        _indoorsSurfaceViewController!.loadBuilding(withBuildingId: 123456789)
    }
    
    func addSurfaceAsChildViewController () {
        self.addChildViewController(_indoorsSurfaceViewController!)
        _indoorsSurfaceViewController!.view.frame = self.view.frame
        self.view.addSubview(_indoorsSurfaceViewController!.view)
        _indoorsSurfaceViewController!.didMove(toParentViewController: self)
    }
}

extension ViewController {
    // MARK: ISIndoorsSurfaceViewControllerDelegate
    
    func indoorsSurfaceViewController(_ indoorsSurfaceViewController: ISIndoorsSurfaceViewController!, isLoadingBuildingWithBuildingId buildingId: UInt, progress: UInt) {
        NSLog("Building loading progress: %lu", progress)
    }

    func indoorsSurfaceViewController(_ indoorsSurfaceViewController: ISIndoorsSurfaceViewController!, didFinishLoading building: IDSBuilding!) {
    }

    func indoorsSurfaceViewController(_ indoorsSurfaceViewController: ISIndoorsSurfaceViewController!, didFailLoadingBuildingWithBuildingId buildingId: UInt, error: Error!) {
        print("Loading building failed with error: \(error)")

        let alert = UIAlertController(title: "Oops!", message: error!.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: IndoorsServiceDelegate

    func onError(_ indoorsError: IndoorsError!) {
        
    }
    
    func locationAuthorizationStatusDidChange(_ status: IDSLocationAuthorizationStatus) {
        
    }
    
    func bluetoothStateDidChange(_ bluetoothState: IDSBluetoothState) {
        
    }
}
